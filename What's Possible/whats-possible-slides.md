---
title: OpenLab | What's Possible?
type: slide
slideOptions:
  transition: slide
slideLink: https://md.opensourceecology.de/p/l66ucw-4e#/
...

<img src="https://gitlab.com/openlab-medtec/lab-logo/uploads/e519f3e851b4aeb9d765d7eda3b3072f/OpenLab-MedTec_Logo-HSU-mono-red.svg" style="border: none;background: none;box-shadow:none" height="600">

---

# Intro-Workshop
## What's Possible?

    Open Educational Resources (OER)
    lizenziert unter CC BY 4.0

    [18.07.2023 - Martin Häuer, Jan Foxley]

<!--- 10min input + 5min Q&A -->

---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

## Was ist Open-Source?

**= geteiltes Wissen**

- <span>für jeden<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>für immer<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>überall<!-- .element: class="fragment" data-fragment-index="3" --></span> 

Note:
    - erst durch die Lizenz wird etwas open-source
    - unwiderruflich, keine geografische Einschränkung, diskriminierungsfrei (jeder darf alles machen)

---

<img src="https://upload.wikimedia.org/wikipedia/commons/8/80/Wikipedia-logo-v2.svg" style="border: none;background: none;box-shadow:none" height="380">

Note:
    - Wikipedia is an open source encyclopaedia which outcompeted all these super-expensive printed versions - still remember them?
    - As content is open source there, everyone can make use of it as he/she likes. You could even make a collection of your favourite articles, print and sell them as a book.
    - Also it's software base - Wikimedia - is open source.

---

<span>![](https://upload.wikimedia.org/wikipedia/commons/3/3a/Tux_Mono.svg =214x258)<!-- .element: class="fragment" data-fragment-index="1" --></span>

|       |                      | 
| ----- | -------------------- | 
| 100 % | Supercomputer        | 
| 95 %  | Server (Top 1 Mio.)  |
| 75 %  | Mobilgeräte          |
| 70 %  | Embedded Systems     |

<!---
sources:
https://itsfoss.com/linux-runs-top-supercomputers/ bzw. https://www.top500.org/statistics/details/osfam/1/
https://www.zdnet.com/article/can-the-internet-exist-without-linux/
https://www.embedded.com/wp-content/uploads/2019/11/EETimes_Embedded_2019_Embedded_Markets_Study.pdf
https://gs.statcounter.com/os-market-share/mobile/worldwide
-->

Note:
    - You probably heard of the largest software project of human history, the kernel of the by far most used operating system, powering xx % of all xx.
    - Linux.

---

<span>Proprietäte software
    enthält **40…60%** Free/Open-Source-Code.<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<div></div>
<span>Niemand schreibt heute noch Software 'from scratch'.<!-- .element: class="fragment" data-fragment-index="2" --></span> 
<div></div>
<span>Dasselbe kann/wird für Hardware passieren.<!-- .element: class="fragment" data-fragment-index="3" --></span> 

<!---
sources:

Flexera Report „State of Open Source License Compliance 2020“
https://www.zdnet.com/article/60-percent-of-codebases-contain-open-source-vulnerabilities/
https://www.helpnetsecurity.com/2018/05/22/open-source-code-security-risk/
-->

---

## Was ist Open-Source-++Hardware++?
---

<span>"[…] Hardware, deren Baupläne öffentlich zugänglich gemacht wurden, so dass alle sie
    studieren, verändern, weiterverbreiten
    und sie sowie darauf basierende Hardware
    herstellen und verkaufen können."<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<span>([OSHWA](https://www.oshwa.org/definition/german/))<!-- .element: class="fragment" data-fragment-index="1" --></span> 

Note:
    Definition der Open Source Hardware Association

---

### :white_check_mark: Grundlagen geklärt

--

<iframe src="https://giphy.com/embed/fs6e9kDwsta5QnGhJh" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

---

## Open-Source-Hardware
## in der Praxis
---

---

<!-- .slide: data-background="https://upload.wikimedia.org/wikipedia/commons/d/db/Reprap_Darwin_2.jpg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [RepRap](https://reprap.org/)

</div>

Note:
    - Polymer-3D-Druck existiert seit den 80ern (zu teuer, zu klobig)
    - Anfang der 2000er liefen die meisten Patente ab
    - Desktop-Version an der University of Bath
    - Bereitstellung der techn. Dokumentation unter Open-Source-Lizenz; und dann ist das hier passier:

---

<!-- .slide: data-background="https://cloud.opensourceecology.de/s/bCAoLFcKqK2bxnM/preview" data-background-color="#000" -->

<!--- source: https://reprap.org/mediawiki/images/e/ec/RFT_timeline2006-2012.png -->

Note:
    - Auschnitt zeigt etwa ¼ des Gesamt-"Stammbaumes"
    - …und auch nur bis 2012
    - Prusa Mendel ist oben rechts im Bild

---

<!-- .slide: data-background="https://prusament.com/media/2021/12/Prusament_PPE_CS-1.jpg" data-background-color="#000" -->

Note:
    3D-Druck:
    - global entwickeln
    - lokal herstellen

---

<!-- .slide: data-background="https://glia.org/cdn/shop/products/GliaStethoscope-RTA-Red_720x.png" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Glia Stethoscope](https://github.com/GliaX/Stethoscope)

</div>

Note:
    - herstellbar zu ~5€
    - selbe Leistungsfähigkeit wie das Littmann Cardiology III (300€)
    - hält eine Kanadische "Medical Devices Establishment Licence [to] manufacture for distribution"
    - entwickelt für die humanitäre Hilfe im Gaza-Krieg
    
<!---
Link zur Publikation zur Leistungsfähigkeit: https://doi.org/10.1371/journal.pone.0193087
MDEL: https://health-products.canada.ca/mdel-leim/ ; ID: 6823
Hintergrundartikel: https://www.aljazeera.com/features/2017/9/5/using-3d-printers-to-tackle-gazas-medical-shortages
-->

---

<!-- .slide: data-background="https://miro.medium.com/v2/resize:fit:1400/format:webp/1*zPPh2vj1XEsGV5JH9ziilw.jpeg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Glia Tourniquet](https://github.com/GliaX/tourniquet)

</div>

Note:
    - Adernpressen zur Erstversorgung
    - ebenfalls ca. 5€ Herstellungskosten
    - entwickelt für die humanitäre Hilfe im Gaza-Krieg
    - hält eine Kanadische "Medical Devices Establishment Licence [to] manufacture for distribution"

<!---
MDEL: https://health-products.canada.ca/mdel-leim/ ; ID: 6823
Hintergrundartikel: https://trklou.medium.com/3d-printed-open-source-tourniquet-rationale-failure-analysis-and-proposed-next-steps-of-the-glia-97e8441b4c5a
-->

---

<!-- .slide: data-background="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/Safecast_bGeigie_Nano_opened.jpg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Safecast](https://safecast.org/)

</div>

![image alt](https://upload.wikimedia.org/wikipedia/en/7/77/Safecast_Tile_Map_screenshot.jpeg) Fukushima crisis response

Note:
    - Ursprung in 'nem Hackerspace in Tokyo, um belastbare Strahlungsdaten aufzustellen
    - v0.1: iPhone vor 'nen Geigerzähler geklemmt (Bilder von der Anzeige + Geokoordinaten)
    - Open-Source-Projekt, sodass Elektrotechniker, Strahlungsphysiker etc. eingebunden werden konnten
    - dezentrale Entwicklung, Modifikation, Herstellung, Anwendung

    learnings:
    
    - man muss nicht alles wissen
    - man muss nicht alles können
    - …aber man muss drüber reden!

<!---
Link zum Talk: https://media.ccc.de/v/29c3-5140-en-safecast_h264
-->


---

## Motivation
---

### <span>1. Impact<!-- .element: class="fragment" data-fragment-index="1" --></span>

- <span>Qualität<!-- .element: class="fragment" data-fragment-index="2" --></span>
    <span>(Peer-Reviews, Spezialwissen)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>Dezentralität<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>Langlebigkeit<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>Community (Partizipation, soziales)<!-- .element: class="fragment" data-fragment-index="5" --></span>

Note:
    Was alle antreibt: Impact. Wenn man wirklich an der Lösung eines Problems interessiert ist, kann Open-Source ein mächtiger Hebel sein.

---

## Motivation
---

    [extrinsisch]

- <span>Externalisierung von Kosten (Facebook)<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>kostenfreies Fachwissen (statt Übernahme)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>Marktpenetration (Android)<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>freie Lieferketten (CERN)<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>Headhunting<!-- .element: class="fragment" data-fragment-index="5" --></span>

<!---
Facebooks Open Compute Project: https://www.opencompute.org/
Android historical market share: https://upload.wikimedia.org/wikipedia/commons/a/ae/World-Wide-Smartphone-Market-Share.png
CERN-Artikel zu OSH: https://home.cern/news/news/knowledge-sharing/making-hardware-design-open-and-free
Headhunting-Studie: 
--->

---

### …und wo passiert das alles?

---

<!-- .slide: data-background="https://pbs.twimg.com/media/E16jnPrXoAA0en1.png" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

<img src="https://git-scm.com/images/logos/downloads/Git-Logo-1788C.png
" style="border: none;background: none;box-shadow:none" height="100">

</div>

Note:
    - in erster Linie online
    - git = dezentrale Versionskontrolle (um den Überblick bei all den Beiträgen, Änderungen und Versionen zu behalten)

---

<img src="https://gitlab.com/openlab-medtec/lab-logo/uploads/e519f3e851b4aeb9d765d7eda3b3072f/OpenLab-MedTec_Logo-HSU-mono-red.svg" style="border: none;background: none;box-shadow:none" height="200">

<iframe src="https://giphy.com/embed/2t83pnFDUIkYp6HnVG" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

---

## Was haben wir im OpenLab?
---

- ++vor Ort:++
    - <span>3D-Drucker (FDM & SLA),<!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>CNC-Fräse,<!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>Elektro- & mech. Werkzeug<!-- .element: class="fragment" data-fragment-index="1" --></span>
- ++Partner:++ <span>16 OpenLabs in Hamburg + PTB<!-- .element: class="fragment" data-fragment-index="2" --></span>
- ++global:++ <span>Open-Source-Communty<!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>IT-Infrastruktur (Matrix, GitLab,…)<!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>Fachwissen, Kontakte<!-- .element: class="fragment" data-fragment-index="3" --></span>

---

## Was wirst Du machen?
---

<span>Wir sind gespannt :)<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## Was wir schon gemacht haben:
---

---

# Projekte
---

* Makercoin
* CNC-Fräse
* CT zu MKG

---

## Makercoin
---

* Brauch der Maker-Community
* Zeigt technische Fähigkeiten
* Visitenkarte
* kleines Geschenk

<img src="https://d31ezp3r8jwmks.cloudfront.net/ogATHwKCgNa6b9dyevMuuACY" height="120">

    Makercoin von "Makers Muse"

---

## Was soll es machen?
---

* Fahigkeiten zeigen
* kompakt sein
* wiedererkennbar sein


<img src="https://www.3dmake.net/wp-content/uploads/2018/10/3d-modell-maker-coin-3d-model.jpg" height="150">

    Makercoin von "VickyTGAW"

---

## Unser Anspruch?
---

* Es soll kein sinnloses Stück Plastik sein!
* Es soll mit medizintechnik zu tun haben!
* Wir möchten ein kleines Dankeschön an Redner und fleißige Unterstützer verteilen.

---

## Wie erreichen wir das?
---

Komplette Neuentwicklung?
Zu aufwendig für den Zweck.
Können wir etwas Gutes kompakt machen?
Wir überdachten das Open Source Projekt Glia-Stethoskop.

<img src="https://glia.org/cdn/shop/products/GliaStethoscope_720x.png?v=1663651975" height="120">

    Glia Stethoskop by Glia Equal Care

---

# CNC-Fräse
---

* Fundobjekt in regionaler Werkstatt
* nie wirklich genutzt, da kompliziert
* "Dann kann es genauso gut hier herumstehen"

---

# CT zu MKG
---

SA Alexander Schalles hatte eine Idee:
Was können wir mit unseren CT-Daten noch machen?
Das sind doch quasi 3D-Modelle!

---

## Was kann man damit machen?
---

* Modelle zur Schulung und Anschauung
* Maßstabsgetreue Modelle zur OP-Vorbereitung

---

## Was haben wir schon damit gemacht?
---

**Meloblastom im Unterkiefer**

* 3D-Druck des modellierten Unterkiefers
* Vorbiegemodell für Ostheosyntheseplatte
* OP- und damit Narkosezeit konnte erfolgreich verkürzt werden!

---


**Bruch im Orbitaboden**

<img src="https://www.ksw.ch/app/uploads/2018/08/schaedel-orbitaboden.jpg" height="150">

* CT-Scan der gesunden Seite
* 3D-Modellierung und Spiegelung der Augenhöhle
* 3D-gedrucktes Modell diente zum Vorbiegen des Titan-Mesh
* OP- und damit Narkosezeit konnte erfolgreich verkürzt werden!
* Verbesserung des Ergebnisses, da Modellierung ohne störende Strukturen (Rest des Kopfes) möglich ist

---

# Q&A
---
**Habt ihr noch Fragen?**

---

<!-- .slide: data-background-color="#FFFFFF" -->

<img src="https://gitlab.com/openlab-medtec/workshops/-/raw/main/What's%20Possible/openlab-medtec-qr-code-logo.png" style="border: none;background: none;box-shadow:none" height="400">

see you on the other side :)
