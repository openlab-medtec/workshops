# Workshop-Material

## Intro

Dieses Repository enhält die Materialien der im [OpenLab MedTec](https://openlab.hamburg/de/openlabs/medtec/) durchgeführten Einführungsworkshops.

## Inhalt

### What's Possible?

- Material: [/What's Possible](/What's%20Possible)
- [Slideshow](https://md.opensourceecology.de/p/l66ucw-4e#/)

### Ideation

coming soon

### 3D-Druck

coming soon

### Fräsen

coming soon

## Lizenz

Alle Inhalte des Repos sind unter [CC BY 4.0](LICENSE.md) lizenziert und damit open-source. Lizenzgeber ist, sofern nicht anders angegeben das [Bundeswehrkrankenhaus Hamburg](https://www.bwkrankenhaus.de/de/bundeswehrkrankenhaus-hamburg).
