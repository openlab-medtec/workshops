---
title: Grundlagen OSH @`OpenLab MedTec`
type: slide
slideOptions:
  transition: slide
slide show: https://md.opensourceecology.de/p/e8kg_9cxC#/
...

<img src="https://gitlab.com/openlab-medtec/lab-logo/uploads/e519f3e851b4aeb9d765d7eda3b3072f/OpenLab-MedTec_Logo-HSU-mono-red.svg" style="border: none;background: none;box-shadow:none" height="600">

---

Entwickeln von

# Open-Source-Hardware
---

    (Open Educational Resources (OER))
    lizenziert unter CC BY 4.0, Martin Häuer

<!--- 

10min Intro
2min Q&A
15min legal
5min Q&A
10min Doku
5min Q&A
15min Versionierung & Plattformen
5min Q&A

10min Puffer

∑=(67+10)min

-->

---

### Hardware → <span><!-- .element: class="fragment highlight-red" data-fragment-index="1" -->Open</span>-<span><!-- .element: class="fragment highlight-red" data-fragment-index="2" -->Source</span>-Hardware

↓

braucht 3 Dinge:

1. <span>Lizenz<!-- .element: class="fragment" data-fragment-index="1" --></span>
2. <span>technische Dokumentation<!-- .element: class="fragment" data-fragment-index="2" --></span>
3. <span>Plattform<!-- .element: class="fragment" data-fragment-index="3" --></span>

---

## Agenda
---

0. <span>Intro OSH<!-- .element: class="fragment" data-fragment-index="2" --></span> 
1. <span>Rechtsgrundlage<!-- .element: class="fragment" data-fragment-index="1" --></span> 
2. <span>Technische Dokumentation<!-- .element: class="fragment" data-fragment-index="1" --></span> 
3. <span>Dezentrale Versionskontrolle<!-- .element: class="fragment" data-fragment-index="1" --></span> 
4. <span>++Bonus:++<!-- .element: class="fragment" data-fragment-index="3" --></span><span>  Geschäftsmodelle<!-- .element: class="fragment" data-fragment-index="3" --></span> 

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZXQ2aHhncmh3YTJneWJkdHJhYmUzbmQ0aGs2cnA3aHZucjJwMWpkdSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/QMMt03hAmVbNu/giphy-downsized.gif" style="border: none;background: none;box-shadow:none" height="400">

---

<img src="https://www.oshwa.org/wp-content/uploads/2014/03/oshw-logo.svg" style="border: none;background: none;box-shadow:none" height="600">

---

## Was ist Open-Source?
---

**= geteiltes Wissen**

- <span>für jeden<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>für immer<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>überall<!-- .element: class="fragment" data-fragment-index="3" --></span> 

Note:
    - erst durch die Lizenz wird etwas open-source
    - unwiderruflich, keine geografische Einschränkung, diskriminierungsfrei (jeder darf alles machen)

---

## The 4 Freedoms
---

Jeder darf Open-Source-Technologie

- <span>nutzen<!-- .element: class="fragment" data-fragment-index="1" --></span> 
- <span>studieren<!-- .element: class="fragment" data-fragment-index="2" --></span> 
- <span>weiterentwickeln<!-- .element: class="fragment" data-fragment-index="3" --></span> 
- <span>weiterverbreiten<!-- .element: class="fragment" data-fragment-index="4" --></span> 

<span>…und zwar in jeglicher Form (diskriminierungsfrei)<!-- .element: class="fragment" data-fragment-index="5" --></span> 

Note: 
    Also auch verkaufen

---

<img src="https://upload.wikimedia.org/wikipedia/commons/b/b0/NewTux.svg" style="border: none;background: none;box-shadow:none" height="200">

|       |                      | 
| ----- | -------------------- | 
| 100 % | Supercomputer        | 
| 95 %  | Server (Top 1 Mio.)  |
| 75 %  | Mobilgeräte          |
| 70 %  | embedded systems     |

<!---
sources:
https://itsfoss.com/linux-runs-top-supercomputers/ bzw. https://www.top500.org/statistics/details/osfam/1/
https://www.zdnet.com/article/can-the-internet-exist-without-linux/
https://www.embedded.com/wp-content/uploads/2019/11/EETimes_Embedded_2019_Embedded_Markets_Study.pdf
https://gs.statcounter.com/os-market-share/mobile/worldwide
-->

Note:
    - You probably heard of the largest software project of human history, the kernel of the by far most used operating system, powering xx % of all xx.
    - Linux.

---

<span>Proprietäte software
    enthält **40…60%** Open-Source-Code.<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<div></div>
<span>Niemand schreibt heute noch Software 'from scratch'.<!-- .element: class="fragment" data-fragment-index="2" --></span> 
<div></div>
<span>Dasselbe kann/wird für Hardware passieren.<!-- .element: class="fragment" data-fragment-index="3" --></span> 

<!---
sources:

Flexera Report „State of Open Source License Compliance 2020“
https://www.zdnet.com/article/60-percent-of-codebases-contain-open-source-vulnerabilities/
https://www.helpnetsecurity.com/2018/05/22/open-source-code-security-risk/
-->

Note:
    And not only that:
    [Q] What do you think is the average percentage of open source code in proprietary code?

---

## Was ist Open-Source-++Hardware++?

<span>"[…] Hardware, deren Baupläne öffentlich zugänglich gemacht wurden, so dass alle sie
    studieren, verändern, weiterverbreiten
    und sie sowie darauf basierende Hardware
    herstellen und verkaufen können."<!-- .element: class="fragment" data-fragment-index="1" --></span> 
<span>([OSHWA](https://www.oshwa.org/definition/german/))<!-- .element: class="fragment" data-fragment-index="1" --></span> 

Note:
    - That's the definition from the Open Source Hardware Association.
    - So no dependencies.
    - You are publishing design files and just anyone could build and sell the hardware.

---

## Open Source Hardware
## in der Praxis
---

---

<!-- .slide: data-background="https://learn.circuit.rocks/wp-content/uploads/2019/12/arduinogardware-e1600234850301.png" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Arduino](https://www.arduino.cc/)

</div>

---

<!-- .slide: data-background="https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/EggPainter.png" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Eggprinter](https://www.thingiverse.com/thing:2245428)

</div>

---

<!-- .slide: data-background="https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/AssemblyInstructions/Assembly.png" data-background-color="#000" -->

<span>![](https://raw.githubusercontent.com/ProbotXYZ/EggBot/master/Samples/More%20Eggbot%20Op%20Art/preview.jpg)<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

<!-- .slide: data-background="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/Safecast_bGeigie_Nano_opened.jpg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Safecast](https://safecast.org/)

</div>

![image alt](https://upload.wikimedia.org/wikipedia/en/7/77/Safecast_Tile_Map_screenshot.jpeg) Fukushima crisis response

---

<!-- .slide: data-background="https://i2.wp.com/fossa.systems/wp-content/uploads/2019/10/66748130_891630314507139_3361097979711717376_n.jpg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [FOSSASAT](https://fossa.systems/fossasat-1/)

</div>

---

<!-- .slide: data-background="https://i.ytimg.com/vi/gXP01bjIwHo/maxresdefault.jpg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [LifeTrac](https://wiki.opensourceecology.org/wiki/LifeTrac_6)

Open Source Ecology (US)

</div>

---

<!-- .slide: data-background="https://www.digitaltrends.com/wp-content/uploads/2019/07/farmbot-express-feat-3251132.jpg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Farmbot](https://farm.bot/)

</div>

---

<!-- .slide: data-background="https://embetty.utopix.net/video/youtube/8J7JZcsoHyA-poster-image" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Precious Plastic](https://preciousplastic.com/)

</div>

---

<!-- .slide: data-background="https://glia.org/cdn/shop/products/GliaStethoscope-RTA-Red_720x.png" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Glia Stethoscope](https://github.com/GliaX/Stethoscope)

</div>

Note:
    - herstellbar zu ~5€
    - selbe Leistungsfähigkeit wie das Littmann Cardiology III (300€)
    - hält eine Kanadische "Medical Devices Establishment Licence [to] manufacture for distribution"
    - entwickelt für die humanitäre Hilfe im Gaza-Krieg
    
<!---
Link zur Publikation zur Leistungsfähigkeit: https://doi.org/10.1371/journal.pone.0193087
MDEL: https://health-products.canada.ca/mdel-leim/ ; ID: 6823
Hintergrundartikel: https://www.aljazeera.com/features/2017/9/5/using-3d-printers-to-tackle-gazas-medical-shortages
-->

---

## OSH <span><!-- .element: class="fragment highlight-red" -->≠</span> DIY

Note:
    - Even though lots of OSH projects are designed to be replicated by anyone, I want to point out that OSH IS NOT DIY
    - DIY:
      - limited to what you can do yourself
      - often unclear license
      - commercial use not considered

---

| Unternehmen | Jahresumsatz   |
| ----------- | -------------- |
| Arduino     | `$` 161.9m     |
| Sparkfun    | `$` 72.6m      |
| Prusa       | `$` 50.2m      |
| …           |                |

<!---

sources: 

https://ecommercedb.com/en/store/arduino.cc
https://ecommercedb.com/en/store/sparkfun.com
https://www.wikidata.org/wiki/Q27923775

-->

---

<!-- .slide: data-background="https://whitestack.com/wp-content/uploads/2022/02/fb_ocp.jpeg" data-background-color="#000" -->

<div style="background-color: rgba(0, 0, 0, 0.7); color: #fff; padding: 5px;">

## [Open Compute Project](https://www.opencompute.org/)

</div>

---

<iframe width="1024" height="576" data-src="https://www.opensourceimaging.org/"></iframe>

---

## Motivation
---

### <span>1. Impact<!-- .element: class="fragment" data-fragment-index="1" --></span>

- <span>Qualität<!-- .element: class="fragment" data-fragment-index="2" --></span>
    <span>(Peer-Reviews, Spezialwissen)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>Dezentralität<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>Langlebigkeit<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>Community (Partizipation, soziales)<!-- .element: class="fragment" data-fragment-index="5" --></span>

Note:
    Was alle antreibt: Impact. Wenn man wirklich an der Lösung eines Problems interessiert ist, kann Open-Source ein mächtiger Hebel sein.

---

## Motivation
---

    [extrinsisch]

- <span>Externalisierung von Kosten (Facebook)<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>kostenfreies Fachwissen (statt Übernahme)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>Marktpenetration (Android)<!-- .element: class="fragment" data-fragment-index="3" --></span>
- <span>freie Lieferketten (CERN)<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>Headhunting<!-- .element: class="fragment" data-fragment-index="5" --></span>

<!---
Facebooks Open Compute Project: https://www.opencompute.org/
Android historical market share: https://upload.wikimedia.org/wikipedia/commons/a/ae/World-Wide-Smartphone-Market-Share.png
CERN-Artikel zu OSH: https://home.cern/news/news/knowledge-sharing/making-hardware-design-open-and-free
Headhunting-Studie: 
--->

---

## Herausforderungen für's Geschäft:

- <span>weniger Abhängigkeiten<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>experimenteller Weg<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>neue Werkzeuge, Aufgaben und Rechtsfragen<!-- .element: class="fragment" data-fragment-index="3" --></span>

Note: 
    - dependencies are a solid base for commercial business models
    - sacrifice a fairly predictable path
    - …for new tools, tasks and legal issues

---

## Wozu die Mühe?

---

## Was ist der Sinn des Projekts?

---

# Q&A
---

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExems4Z29ybWxvcmV4cjUyYnBodWd2djl1d2FzbmZneDZoa29rYjZ0NSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/VbnUQpnihPSIgIXuZv/giphy-downsized.gif" style="border: none;background: none;box-shadow:none" height="400">

---

# Geistiges Eigentum
---

in Zeiten von Open-Source

---

2 einander ausschließende Rechtsfelder:
(für unsere Perspektive)

## Urheberrecht

## Patentrecht

## <span>+ Markenrecht<!-- .element: class="fragment" data-fragment-index="1" --></span> 

---

---
## Urheberrecht (Copyright)
---

…gibt dem Eigentümer das exklusive Recht, ein <span><!-- .element: class="fragment highlight-red" data-fragment-index="2" -->kreatives</span> Werk zu nutzen
(vorzuführen, weiterzuverbreiten […])
und Kopien davon anzufertigen.


<span>--
    Das offizielle "Teilen" des Werks
    erfordert eine Lizenz.<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    Eine Lizenz ist sozusagen ein Standardvertrag mit Nutzern.
    Du möchtest meinen Film übertragen? Zahle diese Gebühr und halte dich an diese Regeln, dann ist alles in Ordnung.

---

> "[Niemand kann]
> Ideen, Verfahren, Prozesse, Systeme, Betriebsmethoden, Konzepte, Prinzipien oder Entdeckungen
> urheberrechtlich schützen."

    Copyright Act, Abs. 102(b)

Note:
    Dafür haben wir →

---

---
## Patent
---

…gibt dem Eigentümer das exklusive Recht, eine Erfindung herzustellen, zu nutzen oder zu verkaufen.

<span>--
    …übertragbar durch eine Lizenz.<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

### Wie unterscheidet man Produkte von verschiedenen Herstellern?

---

---
## Markenrecht™
---

…identifiziert Produkte/Dienstleistungen aus einer bestimmten Quelle.

|      |      |             |      |
| ---- | ---- | ----------- | ---- |
| Name | Logo | Slogan      | etc. |

<span>OSH-Sammelfiguren können von jedem hergestellt werden,
    aber nur der Markeninhaber
    darf Hulk (oder wer auch immer) verkaufen.<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

## Der konventionelle Weg:

> Wenn du eine großartige Idee hast:
> Dann patentiere sie!

---

<!-- .slide: data-transition="fade" -->

<img class="r-stretch" src="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/forbes-screenshot.png">

([Artikel](https://www.forbes.com/sites/stephenkey/2017/11/13/in-todays-market-do-patents-even-matter/#4e2c9d2c56f3))

---

<!-- .slide: data-transition="fade" -->

## Warum?

Note:
    [Diskussion] 1…5min

---

- Erfindungen müssen neuartig sein
- Patente sind kostenintensiv
- Patentieren dauert

---

> "Alles, was erfunden werden kann, ist erfunden worden."

    Henry L. Ellsworth, Kommissar des US-Patentamts,
    Bericht an den Kongress von 1843

<!--- Quelle:
https://en.wikiquote.org/wiki/Patent#Misattributed :)
-->

Note:
    Nun, wir wissen alle, dass das nicht wahr ist - besonders im Jahr 1843.
    Aber da patentierbare Ideen _neu_ sein müssen, ohne ein bestehendes Patent zu verletzen...

---

Nach fast 550 Jahren Patentrecht

<img src="https://gitlab.com/osh-academy/osh-basics/-/raw/master/graphics/noun_Missing Puzzle Piece_1445716.svg" style="border: none;background: white;box-shadow:none" height="400">

sind patentierbare Lücken selten geworden.

<span>+ Patente sind nicht kostenlos<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- Quelle:
https://en.wikipedia.org/wiki/History_of_patent_law#cite_note-1
-->

Note:
    dennoch wurden noch nie so viele Patente angemeldet wie heute

---

Ein Patent für Hauptmärkte <span>anmelden<!-- .element: class="fragment highlight-red" data-fragment-index="3" --></span>

(EU,    GB,    CN,    JP,    US)

=

### <span>30.000…100.000+<!-- .element: class="fragment" data-fragment-index="1" --></span> €

`...pro zu schützende Idee.`

<span>--
    Man vergewissere sich vorher, dass man ein Patent braucht.<!-- .element: class="fragment" data-fragment-index="2" --></span>

<!--- Quelle:
[Deutsch] https://www.patent-pilot.com/de/ein-patent-anmelden/kosten-der-patentanmeldung/
-->

Note:
    - Patente gelten national, daher in jeder Region einreichen
    - [Q] Rate die Kosten

---

die Nutzung eines Patents

im Rechtsstreit

=

### 100.000…30.000.000 € 

<span>--
    Man vergewissere sich, dass man das Patent auch einsetzen kann.<!-- .element: class="fragment" data-fragment-index="1" --></span>

<!--- Quelle:
https://preubohlig.de/wp-content/uploads/2019/07/PatentLitigationHoppe.pdf
https://apnews.com/press-release/news-direct-corporation/a5dd5a7d415e7bae6878c87656e90112
-->

Note:
    Durchschnitt=3,6 Mio. €
    Ähnlich in den USA

---

Nach der Anmeldung dauert das Verfahren
<span>3…5 Jahre<!-- .element: class="fragment" data-fragment-index="1" --></span> (in der EU).

<span>vs.
    Produktlebensdauer<!-- .element: class="fragment" data-fragment-index="2" --></span>

Note:
    [Q] Wie lange dauert der Prozess?
    Lebensdauer von Konsumgütern: ~18 Monate

<!--- Quelle:
https://www.epo.org/service-support/faq/own-file.html
-->

---

## Aber

"Patente geben dir <span>eine Garantie im Rechtsstreit."<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span>

<span>einen Platz am Tisch, sowohl in der Offensive als auch in der Defensive. Nichts weiter."<!-- .element: class="fragment" data-fragment-index="1" --></span>
    
([Forbes](https://www.forbes.com/sites/stephenkey/2017/11/13/in-todays-market-do-patents-even-matter/?sh=2edbefab56f3))

<span>

        "[…] 2010–2012, 45% of patents were […]
        fully invalid and 33% […] partially"
<!-- .element: class="fragment" data-fragment-index=21" --></span>


([EJLE](https://link.springer.com/article/10.1007/s10657-019-09627-4))

Note:
    Leider ist das nicht der Fall.
    Der Wert (und die Legitimität) eines Patents zeigt sich im Prozess.
    **[Q]** Was ist der offensive/defensive Gebrauch?

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZmp2NHdrYXJoMHBsemVlNHcyM2J2ZnNrYXFyNGVud241bjM0cTZraCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/kgHj1Jtzga4UO7DmLC/giphy.gif" style="border: none;background: none;box-shadow:none" height="400">

---

## Ein Wort zum Markenrecht:

- <span>territorial (wie Patente)<!-- .element: class="fragment" data-fragment-index="1" --></span>

- <span>Anmeldung = 250…1.000 €,
  Prozesskosten = 25.000…300.000 €<!-- .element: class="fragment" data-fragment-index="2" --></span>

- <span>= exklusives Verkaufsrecht unter der Marke
  (auch bei Open-Source-Produkten)<!-- .element: class="fragment" data-fragment-index="3" --></span>

<!--- Quelle:
https://www.uspto.gov/trademark/trademark-fee-information
https://euipo.europa.eu/ohimportal/en/fees-and-payments
https://www.2s-ip.com/ip-it-wissen/markenrecht/markenrecht-a-z/prozesskosten/
https://www.trademarkbob.com/blog/trademark-litigation-costs/
-->

---

|  | Urheberrecht | Patent | Markenrecht |
| -------- | -------- | -------- | ----- |
| betrifft | kreatives Werk | Erfindung | Identität |
| gilt | automatisch | bei Anmeldung | bei Registrierung |
| Kosten | - | €€€ | € |
| Gültigkeit | ~100 Jahre | ≤20 Jahre | ∞ |

---

# Opening the Source
---

Wie wird aus Hardware Open-Source-Hardware?

---

### Hardware

=

- <span>funktionale Teile<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>technische Dokumentation<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>Software<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>Kunst<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>…<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

| Patentrecht | ~~~ |  | Urheberrecht |
| -------- | -------- | -------- | -------- |
| funktionale Teile | technische Dokumentation | Software | Kunst |

Note:
    - Links: standardmäßig _offen_, es sei denn, es gibt ein Patent
    - Rechts: standardmäßig _geschlossen_, wir benötigen eine Lizenz, um sie zu öffnen
    - Die unscharfe Linie zwischen beiden Konzepten liegt irgendwo in der Dokumentation

---

Da funktionale Teile _nicht_ durch eine
Open-Source-Lizenz geschützt sind…

### <span>Wie kann ich andere daran hindern, meine Erfindung zu patentieren?<!-- .element: class="fragment" data-fragment-index="1" --></span>

---

Patentfähige Ideen müssen

- <span>neuartig sein<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>,
- nicht offensichtlich,
- nützlich

<span><!-- .element: class="fragment" data-fragment-index="2" --></span>

### <span>Niemand kann den 'Stand der Technik' patentieren.<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

## Defensive Publishing
---

Eine Idee, die in <span>...<!-- .element: class="fragment fade-out" data-fragment-index="1" --></span> <span>einem beliebigen Teil der Welt<!-- .element: class="fragment" data-fragment-index="1" --></span> veröffentlicht wurde,

kann in Deutschland nicht mehr patentiert werden. <span>Oder anderswo.<!-- .element: class="fragment" data-fragment-index="1" --></span>

### <span>'veröffentlicht'<!-- .element: class="fragment" data-fragment-index="2" --></span>
<span>=<!-- .element: class="fragment" data-fragment-index="2" --></span>
<span>robuster Zeitstempel + öffentlicher Zugang<!-- .element: class="fragment fade-up" data-fragment-index="2" --></span>

Note:
    Etwas, das in Ungarn veröffentlicht wurde, kann in Japan nicht mehr patentiert werden
    Wenn du also nur einmal deinem Freund erzählt hast: kein Zeitstempel, kein öffentlicher Zugang
    Wenn du es auf GitLab veröffentlicht hast: robuster Zeitstempel, öffentlicher Zugang

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOWRuZm01ZDJraXptY2VtN3l2NWt5bno2YmlqZnp1dTlxMTRnbHF3NiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/8hXXilmk33wtmAGJNu/giphy-downsized.gif" style="border: none;background: none;box-shadow:none" height="400">

Note: Man kann sich's auch einfach machen

---

### Wie sieht das in der Praxis aus?

---

<iframe width="1024" height="576" data-src="https://en.oho.wiki/wiki/Parabolic_Solar_Cooker_with_Aluminium_Reflector" style="border: none;background: white;box-shadow:none"></iframe>

---

### Lizenzempfehlungen
---

| copyleft       | <i class="fa fa-creative-commons"></i> | <i class="fa fa-code"></i> | <i class="fa fa-cog"></i> |
| -------------- | ---- | ---- | ---- |
| strong         | [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/legalcode) | [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) | [CERN OHL-S](https://ohwr.org/cern_ohl_s_v2.txt) |
| weak           | - | [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.txt) | [CERN OHL-W](https://ohwr.org/cern_ohl_w_v2.txt) |
| non/permissive | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode) | [MIT](https://opensource.org/license/mit/), [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt) | [CERN OHL-P](https://ohwr.org/cern_ohl_p_v2.txt) |

---

# Q&A
---

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExaXNxbmg4NHBqd2xkdzFieGpwZGJjaGhzbmFoeXg4YnNmaDFza3VjNSZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/CjmvTCZf2U3p09Cn0h/giphy-downsized.gif" style="border: none;background: none;box-shadow:none" height="400">

---

# The Source
### of
## Open Source Hardware

---

[OpenFlexure Microscope](https://openflexure.org/)

<img src="https://openflexure.org/assets/ofm-photos/v7_side_view_crop.jpg" style="border: none;background: none;box-shadow:none" height="350">

---

<img src="https://www.biorxiv.org/content/biorxiv/early/2019/12/03/861856/F2.large.jpg" style="border: none;background: none;box-shadow:none" height="650">

<!--- source: -->

---

- 3D-Modelle
- Elektronik
- Software
- Stückliste
- Bauanleitung
- Gebrauchsanweisung
- Berechnungen, Notizen

…

<span>_…soll ich das alles hochladen?_<!-- .element: class="fragment" data-fragment-index="1" --></span> 

<span>Ja na klar :)<!-- .element: class="fragment" data-fragment-index="2" --></span> 

---

> Die techn. Dokumentation muss genügend Informationen enhalten, sodass die Zielgruppe die Hardware
> - nachbauen und
> - weiterentwickeln kann.

    DIN SPEC 3105-1

---

## Datei-Formate
---

verlustfreie Weiterbearbeitung:

- native Formate
    - CAD: **FreeCAD**<span>, Solidworks, Fusion360<!-- .element: class="fragment" data-fragment-index="1" --></span> 
    - EAD: **KiCAD**<span>, Altium, EAGLE<!-- .element: class="fragment" data-fragment-index="1" --></span> 
    - Text: **Markdown**<span>, LibreOffice Writer,
            MS Word<!-- .element: class="fragment" data-fragment-index="1" --></span> 

---

## Datei-Formate
---

lizenzfreie Austauschformate:

- Export-Formate
    - CAD: JT, STEP, STL
    - EAD: gerber
    - Text: PDF, EPUB

---

Komplexes Beispiel: [OpenFlexure](https://gitlab.com/openflexure/)

Simples Beispiel: [OHLOOM](https://gitlab.com/OSEGermany/ohloom)

Note:
    - wie sieht ein git repo aus? 
        - Lizenz
        - native CAD-Dateien
        - STLs
        - Stückliste
        - Zusammenbau
        - Gebrauchsanweisung


---

# Q&A
---

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExdzhuNXFiZHljZXhjcDU1cXdvb21ranVwazdodnJseXAwd3hhNWZjZCZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/8vQSQ3cNXuDGo/giphy-downsized.gif" style="border: none;background: none;box-shadow:none" height="400">

---

# Hosting the Source
---
Wo hochladen, wo finden?

---

<img src="https://gitlab.com/openlab-medtec/workshops/-/raw/main/Grundlagen%20OSH/wo-hochladen.svg" style="border: none;background: none;box-shadow:none" height="400">

---

# Rollenspiel
---

### Versionskontrolle

Lokal vs. Zentral vs. Dezentral

---

### Runde 1: Lokal
---

Wir brauchen:

- Maintainer (Hauptverantwortlicher)
- Entwickler

---

- `Maintainer`:
    - <span>hat <!-- .element: class="fragment" data-fragment-index="1" --></span><span>[dieses umfangreiche Dokument](https://gitlab.com/openlab-medtec/workshops/-/blob/main/Grundlagen%20OSH/sample-csv.csv)<!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>gibt es weiter an <!-- .element: class="fragment" data-fragment-index="2" --></span><span>`Entwickler`<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Entwickler`:
    - <span>nimmt Änderungen vor<!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>gibt das Dokument zurück an <!-- .element: class="fragment" data-fragment-index="4" --></span><span>`Maintainer`<!-- .element: class="fragment" data-fragment-index="4" --></span>
- `Maintainer`:
    - <span>Welche Änderungen wurden vorgenommen?<!-- .element: class="fragment" data-fragment-index="5" --></span>

---

![](https://gitlab.com/osh-academy/oer-osh-practice/-/raw/master/slides/Screenshot-versioning-sample1.jpg)

---

## Praxisbeispiele
---

- <span>Dokumentation auf USB-Sticks<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>'Änderungen verfolgen' in Textdokumenten<!-- .element: class="fragment" data-fragment-index="2" --></span>

<span>![](https://wiki.documentfoundation.org/images/2/22/Track-changes-Word-2010.jpg)<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

### Runde 2: Zentral
---

Wir brauchen:

- Entwickler 1
- Entwickler 2
- Zentraler Server

---

- `Zentraler Server`:
    - <span>hat <!-- .element: class="fragment" data-fragment-index="1" --></span><span>[dieses umfangreiche Dokument](https://gitlab.com/openlab-medtec/workshops/-/blob/main/Grundlagen%20OSH/sample-csv.csv)<!-- .element: class="fragment" data-fragment-index="1" --></span>
- `Entwickler 1`:
    - <span>fordert Zugang zum Dokument bei <!-- .element: class="fragment" data-fragment-index="2" --></span><span>`Zentralen Server`<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Zentraler Server`:
    - <span>gewährt Zugang an <!-- .element: class="fragment" data-fragment-index="3" --></span><span>`Entwickler 1`<!-- .element: class="fragment" data-fragment-index="3" --></span>
    <span>(Einchecken)<!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>verfolgt Änderungen<!-- .element: class="fragment" data-fragment-index="4" --></span>

…

---

…

- `Entwickler 2`:
    - <span>fordert Zugang bei <!-- .element: class="fragment" data-fragment-index="1" --></span><span>`Zentralem Server`<!-- .element: class="fragment" data-fragment-index="1" --></span>
- `Zentraler Server`:
    - <span>Zugriff verweigert, Datei ist gesperrt<!-- .element: class="fragment" data-fragment-index="2" --></span>
    - <span>Warum? → Konflikte vermeiden!<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Entwickler 1`:
    - <span>fertig (Auschecken)<!-- .element: class="fragment" data-fragment-index="3" --></span>
- `Zentraler Server`:
    - <span>Sehr fein, ciao!<!-- .element: class="fragment" data-fragment-index="4" --></span>

---

## Praxisbeispiele
---

- <span>Cloud<!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>HedgeDoc, Onlyoffice, GoogleDocs (simultane Veränderungen)<!-- .element: class="fragment" data-fragment-index="2" --></span>

---

### Runde 3: Dezentral
---

Wir brauchen:

- Entwickler 1
- Entwickler 2

---

- `Entwickler 1`:
    - <span>hat <!-- .element: class="fragment" data-fragment-index="1" --></span><span>[dieses umfangreiche Dokument](https://gitlab.com/openlab-medtec/workshops/-/blob/main/Grundlagen%20OSH/sample-csv.csv)<!-- .element: class="fragment" data-fragment-index="1" --></span>
- `Entwickler 2`:
    - <span>forkt das Dokument und macht Änderungen<!-- .element: class="fragment" data-fragment-index="2" --></span>
- `Entwickler 1`:
    - <span>macht Änderungen<!-- .element: class="fragment" data-fragment-index="3" --></span>

…

---

…

- `Entwickler 2`:
    - <span>schickt einen 'Merge-Request' an <!-- .element: class="fragment" data-fragment-index="4" --></span><span>`Entwickler 1`<!-- .element: class="fragment" data-fragment-index="4" --></span>
- <span>`git:`<!-- .element: class="fragment" data-fragment-index="5" --></span>
    - <span>automatisches Zusammenführen (wenn möglich)<!-- .element: class="fragment" data-fragment-index="5" --></span>
    - <span>fragt <!-- .element: class="fragment" data-fragment-index="6" --></span><span>`Entwickler 1`<!-- .element: class="fragment" data-fragment-index="6"--></span>
        <span> nach Klärung von unlösbaren Konflikten<!-- .element: class="fragment" data-fragment-index="6" --></span>

---

## Praxisbeispiel
---

<img src="https://git-scm.com/images/logos/downloads/Git-Logo-1788C.png" style="border: none;background: none;box-shadow:none" height="100">

<span>[OpenFlexure Delta Stage](https://gitlab.com/openflexure/openflexure-delta-stage) <!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    go to: Code → Repository Graph

---

## Und wo jetzt hochladen?
---

### Such's Dir aus

---

<iframe width="1024" height="576" data-src="https://wikifab.org/wiki/DIY_Solar_Charger" style="border: none;background: white;box-shadow:none"></iframe>

---

<iframe width="1024" height="576" data-src="https://build.openflexure.org/openflexure-microscope/v7.0.0-beta1/high_res_microscope/actuator_assembly.html" style="border: none;background: white;box-shadow:none"></iframe>

---

### Allgemein:
---

- <span>Erste Wahl: Community-spezifische Plattform <!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>[Lastenrad-Wiki](https://werkstatt-lastenrad.de/) <!-- .element: class="fragment" data-fragment-index="1" --></span>
    - <span>[Paparazzi (Dronen)](https://wiki.paparazziuav.org/wiki/Main_Page) <!-- .element: class="fragment" data-fragment-index="1" --></span>
- <span>die Referenz: <!-- .element: class="fragment" data-fragment-index="2" --></span><span>[git](https://gitlab.com/)<!-- .element: class="fragment" data-fragment-index="2" --></span>
- <span>breite Auswahl: <!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>[Wikifactory](https://wikifab.org/) <!-- .element: class="fragment" data-fragment-index="3" --></span><span>unterstützt die Produktion <!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>[OHO.wiki](https://oho.wiki/) <!-- .element: class="fragment" data-fragment-index="3" --></span><span>bietet Reviews an <!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>[Thingiverse](thingiverse.com/)<!-- .element: class="fragment" data-fragment-index="3" --></span><span>, <!-- .element: class="fragment" data-fragment-index="3" --></span><span>[YouMagine](https://www.youmagine.com/)<!-- .element: class="fragment" data-fragment-index="3" --></span><span> für 3D-Druckteile <!-- .element: class="fragment" data-fragment-index="3" --></span>
    - <span>wissenschaftliche Publikationen
        (<!-- .element: class="fragment" data-fragment-index="3" --></span><span>[glia stethoscope](https://doi.org/10.1371/journal.pone.0193087)<!-- .element: class="fragment" data-fragment-index="3" --><span>)</span><!-- .element: class="fragment" data-fragment-index="3" --></span>

---

### aber Hauptsache:

- hochgeladen
- Open-Source-Lizenz

---

# Q&A
---

---

<img src="https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZDlmcWgybjFtMm45d3k0MnpwdTJveHd4bzhnejBsNzBjMDVqcm4wYiZlcD12MV9pbnRlcm5hbF9naWZfYnlfaWQmY3Q9Zw/znggaLRDqiHPa/giphy-downsized.gif" style="border: none;background: none;box-shadow:none" height="400">

---

# Open-Source-Geschäftsmodelle
---

Note:
    There are zillions of new opportunities when you make things open.
    Those are paths that (in hardware) not too many people have tried before you.
    Stability of business models highly depends on your product and ecosystem.
    and is still subject to research and entrepreneurship

---

### Finde die unbezahlte Tätigkeit:

<span>Entwicklung<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>, Produktion, Reparatur, service, Anpassungen, <span>Dokumetation<!-- .element: class="fragment highlight-red" data-fragment-index="1" --></span>, Recycling, Wiederaufbereitung, Workshops,…

Note:
    Of course, there's more, but you get the point.

---

Es gibt zahlreiche Geschäftsmodelle. Gründer

# kombinieren

diese üblicherweise

<span>Einige Beispiele zur Inspiration:<!-- .element: class="fragment" data-fragment-index="1" --></span>

Note:
    - All of them come with tensions
    - Look it up at home, check the examples

---

## Open Innovation

der Klassiker
##
---

    …für aktive Communities

([Local Motors](https://en.wikipedia.org/wiki/Rally_Fighter))

    Problem:
    
    zahlt keine Rechnungen
    + kompetitiver "Aurmerksamkeitsmarkt"

---

## Öffentliche Förderung

public money, public code, public hardware

## 
---

    …für gemeinnützige Träger
    
([RepRap](https://researchportal.bath.ac.uk/en/publications/reprap-the-replicating-rapid-prototyper))

    Problem:
    
    man braucht (fast immer) einen gemeinnützigen Träger

---

## Aufpreis 

auf Produkte/Services

## 
---

    …für Spezialwissen/spezielle Fertigungsmethoden

([RedHat](https://www.redhat.com/en/technologies/all-products))

    …für starke Marken

([Arduino](https://store.arduino.cc/))

    Problem:
    
    simple Produkte & intuitive Dokumentation
    → DIY

---

## Direktentwicklung

open-source als Vertragsgegenstand

## 
---

    …für Nischen`
    
([CERN](https://cds.cern.ch/record/2109248/files/CERN-Brochure-2015-002-Eng.pdf))

    Problem:
    
    …für Nischen

---

## Open Core

& proprietäre Addons

## 
---

    …für Produkte/Services, die sich bei Skalierung verändern

([GitLab Enterprise](https://about.gitlab.com/pricing/))

    Problem:
    
    Community entwickelt das Kernprodukt, 
    aber die coolen Features sind Premium??

---

## Pay-Per-Bug/-Feature

statt Service-Vertrag

## 
---

    …Externalisierung der Service- & Entwicklungskosten

([Nextcloud](https://hackerone.com/nextcloud?type=team))

    Problem:
    
    keine bugs, kein Einkommen
    weniger Kontrolle über Produktentwicklung

Note:
    lower support costs by improving your product

---

## Mitgliedschaften

oder Dauerspenden

## 
---

    …für altruistische Projekte mit großer Nutzerbasis

([FreeCAD](https://www.patreon.com/yorikvanhavre))

    Problem:
    
    Spendenmarkt ist hochkompetititv

---

## Crowdfunding
---

    …für virale Ideen

([Flipper Zero](https://www.kickstarter.com/projects/flipper-devices/flipper-zero-tamagochi-for-hackers))
    
    Problem:
    
    Kurzzeit-Finanzierung mit signifikantem PR-Aufwand

<!--- source:
https://www.kickstarter.com/projects/flipper-devices/flipper-zero-tamagochi-for-hackers
-->

Note:
    That's a kickstarter project that asked for 60k and got 4m


---

## Kostenteilung

unter den Nutzern der Technologie
## 
---

    …für kritische Basistechnologien

([Open Compute Project](https://www.opencompute.org/membership/membership-organizational-directory))

    Problem:
    
    da erstmal hinkommen

---

## Entscheidungsmacht verkaufen

get a seat at the table
##
---

    …für kritische Basistechnologien

([Eclipse Foundation Strategic Memberships](https://www.eclipse.org/membership/#tab-levels))

    Problem:
    
    Großkunden können sich reinkaufen und
    über Deine Produktstrategie entscheiden

<!---
Katzen-gif

- ran an die Bouletten
    - xxx https://giphy.com/gifs/cats-QMMt03hAmVbNu
    - https://giphy.com/clips/viralhog-viral-hog-cat-struggles-to-steal-food-from-charcuterie-board-aX27AzPikDtP0TBiro
- nicht alles für uns behalten https://giphy.com/clips/storyful-cat-pizza-kitten-rraOTHq4FPmKP4JkFr
- keeping alive xxx https://giphy.com/gifs/cpr-uses-method-znggaLRDqiHPa
- time to go xxx https://giphy.com/gifs/cat-moment-remember-8vQSQ3cNXuDGo
- geek cat xxx https://giphy.com/gifs/computer-cat-wearing-glasses-VbnUQpnihPSIgIXuZv
- look closer https://giphy.com/gifs/cat-kisses-hugs-MDJ9IbxxvDUQM
- omg cat https://giphy.com/gifs/firefly-dodgeball-wash-5i7umUqAOYYEw
- ready cat xxx https://giphy.com/gifs/leroypatterson-cat-glasses-CjmvTCZf2U3p09Cn0h
- lazy cat xxx https://giphy.com/gifs/8hXXilmk33wtmAGJNu man kann sich's auch einfach machen
- flying cat https://giphy.com/gifs/satisfying-oddlysatisfying-oddly-Wsk5V26W0jVYur1f9Z
- amazed cat xxx https://giphy.com/gifs/justviralnet-funny-cat-double-take-kgHj1Jtzga4UO7DmLC

-->